package com.androidatc.weatherapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.nfc.Tag;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidatc.weatherapp.Utils.AnalyseResponse;
import com.androidatc.weatherapp.Utils.AppController;
import com.androidatc.weatherapp.Utils.AsyncResponse;
import com.androidatc.weatherapp.Utils.Configs;
import com.androidatc.weatherapp.Utils.ConnectionDetector;
import com.androidatc.weatherapp.Utils.GetWeather;
import com.androidatc.weatherapp.Utils.RequestWeather;
import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
//import com.rida.ridav2.utils.TimeoutLisener;
import android.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult>, AsyncResponse, com.android.volley.Response.Listener<JSONObject>, com.android.volley.Response.ErrorListener{

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 60000;

    // Geo variables
    protected static final String TAG = "MainActivity";
    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public ProgressDialog prgDialog;
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Represents a geographical location.
     */
    public Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;

    public TextView advice;

    public double longitude;
    public double latitude;

    public RequestWeather requestWeather = new RequestWeather();

    ConnectionDetector connectionDetector = new ConnectionDetector(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestWeather.delegate = this;

        advice = (TextView) findViewById(R.id.tvAdvice);

        // Check whether device is connecting to the internet
        //ConnectionDetector con = new ConnectionDetector(this);
        //if(con.isConnectingToInternet()){
            prgDialog = new ProgressDialog(this);
            // Set Progress Dialog Text
            prgDialog.setMessage("Fetching Weather and location...");
            // Set Cancelable as False
            prgDialog.setCancelable(false);
            // Show Progress Dialog
            prgDialog.show();
            mRequestingLocationUpdates = false;
            // Update values using data stored in the Bundle.
            Log.e(TAG, "!!!!! Update values using data stored in the Bundle !!!!!");
            updateValuesFromBundle(savedInstanceState);

            // Kick off the process of building the GoogleApiClient, LocationRequest, and
            // LocationSettingsRequest objects.
            Log.e(TAG, "!!!!! Kick of Building google api client !!!!!");
            buildGoogleApiClient();
            //checkLocationSettings();
            Log.e(TAG, "!!!!! Create Location Request !!!!!");
            createLocationRequest();
            Log.e(TAG, "!!!!! Building Location Settings Request !!!!!");
            buildLocationSettingsRequest();
            Log.e(TAG, "!!!!! Check Location Settings !!!!!");
            checkLocationSettings();
            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {
                    if(mCurrentLocation == null){
                        if(prgDialog != null){
                            prgDialog.dismiss();
                        }
                        Log.e("MainActivity", "Attemting to navigate to error page");
                        Intent i = new Intent(getApplicationContext(), FailedLocationUpdate.class);

                        startActivity(i);
                        finish();
                    }

                }
            }, SPLASH_TIME_OUT);
//        } else{
//            // Show dialog: Enable internet
//            Toast.makeText(this, "This application needs internet to function properly", Toast.LENGTH_LONG).show();
//            advice.setText("Please enable data or connect to wifi");
//        }

    }


    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                Log.i(TAG, savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING));
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);


        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        //mLocationRequest.set

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                //updateLocationUI();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        //checkLocationSettings();
                        //startLocationUpdates();
                        startLocationUpdates();
                        //updateLocationUI();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
                //setButtonsEnabledState();
            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "Connected to GoogleApiClient");
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        try {
            if (mCurrentLocation != null) {
                // Request location data

                // GetWeather


                // Accuracy lost!
                longitude = Math.round(mCurrentLocation.getLongitude() * 100.0)/100.0;
                latitude = Math.round(mCurrentLocation.getLatitude() * 100.0) / 100.0;
                // execute asynch task
                //requestWeather.execute(""+longitude, ""+latitude);
                if(connectionDetector.isConnectingToInternet()){
                    weatherUpdate(longitude, latitude);
                } else{
                    // Display dialog, could not proceed because no access to internet
                    showAlertDialog();
                    // Get weather using awareness API
//                    Awareness.SnapshotApi.getWeather(mGoogleApiClient)
//                            .setResultCallback(new ResultCallback<WeatherResult>(){
//
//                                @Override
//                                public void onResult(@NonNull WeatherResult weatherResult) {
//                                    if (!weatherResult.getStatus().isSuccess()) {
//                                        Log.e(TAG, "Could not get weather.");
//                                        return;
//                                    }
//                                    Weather weather = weatherResult.getWeather();
//                                    Log.e(TAG, "Weather: " + weather);
//                                }
//                            });
                }
//

            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Message: " + ex.getCause(), Toast.LENGTH_LONG).show();
            Log.e(TAG, "!!!!! Error on retrieving longitude and latitude !!!!!" + "Message: " + ex.getCause() + " " + ex.getMessage());
        }
    }

    private void showAlertDialog(){
        if(prgDialog != null){
            prgDialog.dismiss();
        }
        // Stop location updates
        //stopLocationUpdates();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Failed to connect to internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please enable data or connect to wifi. This will enable us to get the weather on your current loccation")
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        advice.setText("Unable to access internet");
                        //prgDialog.dismiss();
                        stopLocationUpdates();
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }



    @Override
    public synchronized void onLocationChanged(Location location) {
        Log.e(TAG, "!!!!! Location Changed !!!!!");

        //Toast.makeText(MainActivity.this, "Location changed. Longitude: " + location.getLongitude() + " Latitude: " + location.getLatitude(), Toast.LENGTH_LONG).show();

        mCurrentLocation = location;

        // Means Location data was captured
        if (location != null) {
            // Request location data


            // Accuracy lost!
            longitude = Math.round(location.getLongitude() * 100.0)/100.0;
            latitude = Math.round(location.getLatitude() * 100.0) / 100.0;
            // execute asynch task
            //requestWeather.execute(""+longitude, ""+latitude);
            //if(longitude != 0.0 && latitude != 0.0){
            if(connectionDetector.isConnectingToInternet()){
                weatherUpdate(longitude, latitude);
            } else{
                showAlertDialog();
                // Get weather using awareness API
                //GetWeather getWeather = new GetWeather(this, mGoogleApiClient);
//                Awareness.SnapshotApi.getWeather(mGoogleApiClient)
//                        .setResultCallback(new ResultCallback<WeatherResult>(){
//
//                            @Override
//                            public void onResult(@NonNull WeatherResult weatherResult) {
//                                if (!weatherResult.getStatus().isSuccess()) {
//                                    Log.e(TAG, "Could not get weather.");
//                                    return;
//                                }
//                                Weather weather = weatherResult.getWeather();
//                                Log.e(TAG, "Weather: " + weather);
//                            }
//                        });
            }

            //}


        }
        Toast.makeText(this, "Location Changed!",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
                //setButtonsEnabledState();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        //if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        //}
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void processFinish(String output) {
        AnalyseResponse ar = new AnalyseResponse();
        //String data = new RequestWeather()//getWhether(longitude, latitude);
        // Analyse the data
        //Log.e("MainActivity", output);
        //Log.e("MainActivity", "Called: http://api.worldweatheronline.com/premium/v1/weather.ashx?key=7988494f1431465b8c1160323172408&q= " + longitude + "," + latitude + "&num_of_days=1&tp=24&cc=yes&format=xml");
        //HashMap<String, String> weatherData = null;
        //if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            //weatherData = ar.weatherData(output);
        //}
        // Show this details to user
        //Intent i = new Intent(MainActivity.this, CurrentWeather.class);
        //i.putExtra("w", weatherData.get("w"));
        //i.putExtra("c", weatherData.get("c"));
        //i.putExtra("f", weatherData.get("f"));
        //i.putExtra("lo", longitude+"");
        //i.putExtra("la", latitude+"");
        //startActivity(i);
        // close this activity
        //finish();
    }

    public void weatherUpdate(double longitude, double latitude){
        String url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=7988494f1431465b8c1160323172408&q="+longitude+","+ latitude +"&num_of_days=1&tp=24&cc=yes&format=json";
        Log.e(TAG, "!!!!!! URL = " + url + " !!!!!!");
        JsonObjectRequest depositsRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, (String) null, this,
                this);
        depositsRequest.setRetryPolicy(new DefaultRetryPolicy(Configs.RETRY_POLICY_TIME,
                Configs.RETRY_POLICY_COUNT,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(depositsRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (prgDialog != null){
            prgDialog.dismiss();
        }
        Log.e(TAG, error.getMessage() + " " + error.getCause());
        Toast.makeText(this, "Network error occured , " + error.toString(), Toast.LENGTH_LONG).show();
        error.printStackTrace();
    }

    @Override
    public void onResponse(JSONObject response) {
        if(prgDialog != null){
            prgDialog.dismiss();
        }
        try{
            String mydata = response.toString();
            Log.e(TAG, mydata );
            JSONObject JObject = new JSONObject(mydata);
            JSONObject data = JObject.getJSONObject("data");
            JSONArray current_condition = data.getJSONArray("current_condition");
//            JSONObject weatherDescriptionObject = current_condition.getJSONObject(5);
//            JSONArray wd = weatherDescriptionObject.getJSONArray("weatherDesc");
//            String wdescription = wd.getJSONObject(0).getString("value");
            String wdescription = current_condition.getJSONObject(0).getString("weatherDesc");
            String tempC = current_condition.getJSONObject(0).getString("temp_C");
            String tempF = current_condition.getJSONObject(0).getString("temp_F");
            //Move
            Intent i = new Intent(MainActivity.this, CurrentWeather.class);
            i.putExtra("w", wdescription.replace("\"", "").replace(":", "").replaceAll("[{}]", "").replaceAll("[\\[\\]]", "").replace("value", ""));
            i.putExtra("c", tempC);
            i.putExtra("f", tempF);
            i.putExtra("lo", longitude+"");
            i.putExtra("la", latitude+"");
            startActivity(i);
            // close this activity
            finish();
        } catch(Exception ex){
            Log.e("MainActivity", "!!!!! Message: " + ex.getMessage().toString() + " Cause: " + ex.getCause().toString() + " !!!!!");
            Toast.makeText(this, "An error has occured", Toast.LENGTH_LONG).show();
        }
    }

    public void complete(){

    }
}
