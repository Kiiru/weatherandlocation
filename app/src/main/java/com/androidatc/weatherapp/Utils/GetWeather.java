package com.androidatc.weatherapp.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

/**
 * Created by gaddafi on 8/31/17.
 */

public class GetWeather implements ResultCallback<WeatherResult> {

    public Context context;
    public final String TAG = this.getClass().getSimpleName();

    public GetWeather(Context context, GoogleApiClient mGoogleApiClient){
        this.context = context;
        Awareness.SnapshotApi.getWeather(mGoogleApiClient)
                .setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull WeatherResult weatherResult) {
        if (!weatherResult.getStatus().isSuccess()) {
            Log.e(TAG, "Could not get weather.");
            return;
        }
        Weather weather = weatherResult.getWeather();
        Log.e(TAG, "Weather: " + weather);

    }
}
