package com.androidatc.weatherapp.Utils;

/**
 * Created by gaddafi on 8/24/17.
 */

public interface AsyncResponse {

    void processFinish(String output);
}
