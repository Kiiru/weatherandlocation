package com.androidatc.weatherapp.Utils;

import android.os.Build;
import android.support.annotation.RequiresApi;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by gaddafi on 8/24/17.
 */

public class AnalyseResponse {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public HashMap<String, String> weatherData(String infoToProcess){
        HashMap<String, String> hashData = new HashMap<>();

        // Note
        //=======
        // From the api spec, the response can either be json or xml. I chose xml for this project

        String xpathExp = "/data/current_condition";

        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            //System.out.println("\n Finished");

            dBuilder = dbFactory.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(infoToProcess.getBytes(StandardCharsets.UTF_8));
            Document doc = dBuilder.parse(stream);
            doc.getDocumentElement().normalize();

            XPath xPath = XPathFactory.newInstance().newXPath();

            NodeList nodeList = (NodeList)xPath.compile(xpathExp).evaluate(doc, XPathConstants.NODESET);

            for(int i = 0; i < nodeList.getLength(); i++){
                Node nNode = nodeList.item(i);
                //if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                //System.out.println(eElement.getElementsByTagName("returnCode").item(0).getTextContent());
                String tempC = eElement.getElementsByTagName("temp_C").item(0).getTextContent();
                String tempF = eElement.getElementsByTagName("temp_F").item(0).getTextContent();
                String weatherD = eElement.getElementsByTagName("weatherDesc").item(0).getTextContent();
                hashData.put("c", tempC);
                hashData.put("f", tempF);
                hashData.put("w", weatherD);
                //System.out.println("\n Finished");
                //System.out.println(returnValue);
                //}
            }
        } catch(ParserConfigurationException pce){
            //returnValue = pce.getMessage();
        }catch(SAXException se){
            //returnValue += se.getMessage();
        }catch(IOException ioe){
            //returnValue += ioe.getMessage();
        }catch(XPathExpressionException xpe){
            //returnValue += xpe.getMessage();
        }

        return hashData;
    }
}
