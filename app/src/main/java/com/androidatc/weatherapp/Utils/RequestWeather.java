package com.androidatc.weatherapp.Utils;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by gaddafi on 8/24/17.
 */

public class RequestWeather extends AsyncTask<String, String, String> {

    public AsyncResponse delegate = null;

//    public String getWhether(double lon, double lat) {
//        // This a trial api with a max of 500 requests
//        StringBuffer response = new StringBuffer();
//        String url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=7988494f1431465b8c1160323172408&q= " + lon + "," + lat + "&num_of_days=1&tp=24&cc=yes&format=xml";
//        try {
//            URL obj = new URL(url);
//            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//
//            // optional default is get
//            con.setRequestMethod("GET");
//
//            // request header
//            con.setRequestProperty("User-Agent", "Java-Code");
//
//            //int responseCode = con.getResponseCode();
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//
//            String inputLine;
//
//            while ((inputLine = in.readLine()) != null) {
//                response.append(inputLine);
//            }
//
//            in.close();
//
//            return response.toString();
//        } catch (MalformedURLException ex) {
//            ex.printStackTrace();
//        } catch (ProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return response.toString();
//    }


    @Override
    protected String doInBackground(String... strings) {
        StringBuffer response = new StringBuffer();
        String url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=7988494f1431465b8c1160323172408&q= " + strings[0] + "," + strings[1] + "&num_of_days=1&tp=24&cc=yes&format=xml";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is get
            con.setRequestMethod("GET");

            // request header
            con.setRequestProperty("User-Agent", "Java-Code");

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            //return response.toString();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }


//    protected void onProgressUpdate(String... progress) {
//        //setProgressPercent(progress[0]);
//    }

    protected void onPostExecute(String result) {
        //showDialog("Downloaded " + result + " bytes");
        delegate.processFinish(result);
    }

}
