package com.androidatc.weatherapp.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by gaddafi on 8/24/17.
 */

public class CheckPermissions {

    private int RUNTIME_PERMISSION_CODE = 2;
    private Context context;

    public CheckPermissions(Context context){
        this.context = context;
    }

    private boolean isGPSAccessAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isInternetAccessAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }
}
