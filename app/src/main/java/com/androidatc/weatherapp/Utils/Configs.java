package com.androidatc.weatherapp.Utils;

/**
 * Created by ramson on 5/28/2017.
 */

public class Configs {

   // public static final String BASE_URL = "http://52.168.94.223:8090/Service1.svc/";
    //public static final String BASE_URL = "http://192.168.0.158:8016/Service1.svc/";
   public static final String BASE_URL = "http://192.168.0.158:8016/Service1.svc/";
    public static final String LOGIN = BASE_URL+"ousers";
    public static final String STOCK_BATCH = BASE_URL+"stocktakejournal";
    public static final String UPDATE_STOCK_BATCH = BASE_URL+"updatestocktakejournal";
    public static final Integer BACKGROUND_SERVICE_LOOPTIME=60000; //1 min

  // public static final String STOCK_TAKE = BASE_URL+"stocktaking";
    public static final String STOCK_TAKE = BASE_URL+"stocktakinglist";


   public static final String UPDATE_STOCK_TAKE = BASE_URL+"updatestocktakinglist";
   // public static final String UPDATE_STOCK_TAKE = BASE_URL+"updatestocktaking";

    public static final String FETCH_ITEM_WITH_BCODE = BASE_URL+"fetchitemwithbcode";
    public static final String DNS_MAIN_PREF = "DNS_MAIN_PREF";
    public static final String STOCK_TAKE_TEST = " http://sdp.orange.co.ke:8080/DavisShirtliff/DS/authorize/1";
  //public static final String STOCK_TAKE_CWT = BASE_URL+"stocktaking";


    //BATCH STATUS
    public static final String INITIAL = "Initial";
    public static final String START_COUNT_1 = "StartCount1";
    public static final String END_COUNT_1 = "EndCount1";
    public static final String START_COUNT_2 = "StartCount2";
    public static final String COMPLETE = "Complete";
    public static final String ONGOING = "Ongoing";
    public static final int RETRY_POLICY_TIME=5000;
    public static final int RETRY_POLICY_COUNT=5;


}
