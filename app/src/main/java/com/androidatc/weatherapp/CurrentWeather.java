package com.androidatc.weatherapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.Timestamp;

public class CurrentWeather extends AppCompatActivity {

    TextView longitude, latitude, description, celcius, fahrenheit, date, time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);
        longitude = (TextView) findViewById(R.id.edLongitude);
        latitude = (TextView) findViewById(R.id.edLatitude);
        description = (TextView) findViewById(R.id.edDescription);
        celcius = (TextView) findViewById(R.id.edCelcius);
        fahrenheit = (TextView) findViewById(R.id.edFahrenheit);
        date = (TextView) findViewById(R.id.edDate);
        time = (TextView) findViewById(R.id.edTime);

        // get timestamp
        java.util.Date dateToday = new java.util.Date();
        long longTime = dateToday.getTime();
        String ts = new Timestamp(longTime).toString();

        String[] dateTime = ts.split(" ");

        // Get the bundle extras
        Intent intent = getIntent();
        longitude.setText(intent.getStringExtra("lo"));
        latitude.setText(intent.getStringExtra("la"));
        description.setText(intent.getStringExtra("w"));
        celcius.setText(intent.getStringExtra("c"));
        fahrenheit.setText(intent.getStringExtra("f"));
        date.setText(dateTime[0]);
        time.setText(dateTime[1]);
    }
}
